import { Component, OnInit, DoCheck } from '@angular/core';
import { UserService } from "./services/user.service";
import { CategoryService } from "./services/category.service";
import { global } from "./services/global";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService, CategoryService]
})
export class AppComponent{
  public title = 'myblog';
  public identity: any;
  public token: string;
  public url: string;
  public status: string;
  public categories:Object[];

  constructor(
    public _userService: UserService,
    public _categoryService: CategoryService
  ){
    this.url = global.url;
    this.loadUser();
  }

  ngOnInit(){
    this.loadCategories();
  }

  ngDoCheck(){
    this.loadUser();
  }

  loadUser(){
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  loadCategories(){
    this._categoryService.getCategories().subscribe(
      response => {
        if(response.status && response.status == 'success'){
          this.status = 'success';
          this.categories = response.categories;
        }
        else{
          this.status = 'error';
        }
      },
      error => {
        this.status = 'error';

      }
    );
  }

}
