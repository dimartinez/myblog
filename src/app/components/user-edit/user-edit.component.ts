import { Component, OnInit } from '@angular/core';
import { User } from "../../models/user";
import { UserService } from "../../services/user.service";
import { global } from "../../services/global";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [UserService]
})
export class UserEditComponent implements OnInit {

  public user: User;
  public page_title: string;
  public status: string;
  public errors: string[];
  public identity:any;
  public url: string;
  public froala_options: Object;
  public afuConfig: Object;

  constructor(
    private _userService: UserService
  ) { 
    this.page_title = "Actualiza tus datos";
    this.identity = this._userService.getIdentity();
    this.url = global.url;

    this.user = new User(
      this.identity.sub,
      this.identity.name,
      this.identity.surname,
      'ROLE_USER',
      this.identity.email,
      '',
      this.identity.description,
      this.identity.image
    );

    this.froala_options = {
      charCounterCount: true,
      toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
      toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
      toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
      toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
    };

    this.afuConfig = {
      multiple: false,
      formatsAllowed: ".jpg,.png,.jpeg",
      maxSize: "20",
      uploadAPI:  {
        url: global.url + 'user/upload',
        headers: {
          "Authorization" : this._userService.getToken()
        }
      },
      theme: "attachPin",
      hideProgressBar: false,
      hideResetBtn: true,
      hideSelectBtn: false,
      replaceTexts: {
        selectFileBtn: 'Seleccionar Imágenes',
        resetBtn: 'Restablecer',
        uploadBtn: 'Cargar',
        dragNDropBox: 'Arrastrar y Soltar',
        attachPinBtn: 'Subir el avatar de tu usuario',
        afterUploadMsg_success: 'Avatar exitosamente cargado !',
        afterUploadMsg_error: 'Carga de archivo fallido !'
      }
    };
  }

  ngOnInit() {
  }

  onSubmit(form){
    this._userService.update(this.user).subscribe(
      response => {
        if(response.status == "success"){
          this.status = 'success';
          if(response.actual.name){
            this.user.name = response.actual.name;
          }
          if(response.actual.surname){
            this.user.surname = response.actual.surname;
          }
          if(response.actual.email){
            this.user.email = response.actual.email;
          }
          if(response.actual.description){
            this.user.description = response.actual.description;
          }
          if(response.actual.image){
            this.user.image = response.actual.image;
          }
          this.identity = this.user;
          localStorage.setItem('identity', JSON.stringify(this.identity));
        }
        else{
          this.status = 'error';
          this.errors = response.errors;
        }
      },
      error => {
        this.status = 'error';
      }
    );
  }

  avatarUpload(datos){
    let data = JSON.parse(datos.response);
    this.user.image = data.image;
  }

}
