import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { UserService } from "../../services/user.service";
import { CategoryService } from "../../services/category.service";
import { Category } from "../../models/category";
import { global } from "../../services/global";

@Component({
  selector: 'app-category-new',
  templateUrl: './category-new.component.html',
  styleUrls: ['./category-new.component.css'],
  providers: [UserService, CategoryService]
})
export class CategoryNewComponent implements OnInit {
  public page_title: string;
  public token: string;
  public identity: Object;
  public url: string;
  public status: string;
  public errors: string[];
  public category: Category;

  constructor(
    private _userService: UserService,
    private _categoryService: CategoryService,
    private _router: Router,
    private _route: ActivatedRoute

  ) { 
    this.page_title = "Crear nueva categoría";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = global.url;

    this.category = new Category(1, '');

  }

  ngOnInit() {
  }

  onSubmit(form){
    this._categoryService.create(this.token, this.category).subscribe(
      response => {
        console.log(response);
        if(response.status == "success"){
          this.status = 'success';
          this.category = response.category;

          this._router.navigate(['/inicio']);
        }
        else{
          this.status = 'error';
          this.errors = response.errors;
        }
      },
      error => {
        console.log(error);
        this.status = 'error';
      }
    );
  }

}
