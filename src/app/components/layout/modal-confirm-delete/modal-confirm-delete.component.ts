import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'modal-confirm-delete',
  templateUrl: './modal-confirm-delete.component.html',
  styleUrls: ['./modal-confirm-delete.component.css']
})
export class ModalConfirmDeleteComponent implements OnInit {

  @Input() element_id: any;
  @Input() modal_title: string;
  @Input() modal_body: string;
  @Output() deleteElement = new EventEmitter<number>();

  constructor() {
    this.modal_title = "Confimar borrado";
    this.modal_body = "Está seguro de querer borrar permanentemente el elemento. Esta acción no tendrá retroceso.";
  }

  ngOnInit() {
  }

  sendDeleteElement(id:number){
    this.deleteElement.emit(id);
  }

}
