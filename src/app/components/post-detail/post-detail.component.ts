import { Component, OnInit } from '@angular/core';
import { PostService } from "../../services/post.service";
import { Post } from '../../models/post';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { UserService } from "../../services/user.service";
import { global } from "../../services/global";

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
  providers: [PostService, UserService]
})
export class PostDetailComponent implements OnInit {

  public status: string;
  public post: Post;
  public url: string;
  public identity: any;
  public token: string;
  public mycourse: string;
  public myprogram: string;

  constructor(
    private _postService: PostService,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { 
    this.url = global.url;
    this.identity = _userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() {
    this.getPost();
  }

  getPost(){
    this._route.params.subscribe(
      params => {
        let id = +params['id'];

        if(id > 0){
          this._postService.getPost(id).subscribe(
            response => {
              if(response.status == 'success'){
                this.status = 'success';
                this.post = response.post;
              }
              else{
                this.status = 'error';
                this._router.navigate(['inicio']);
              }
            },
            error => {
              this.status = 'error';
              this._router.navigate(['inicio']);
            }
          );
      
        }
        else{
          this.status = 'error';
          this._router.navigate(['inicio']);  
        }
      },
      error => {
        this.status = 'error';
        this._router.navigate(['inicio']);
      }
    );

  }

  onDeletePost(id){
    this._postService.delete(this.token, id).subscribe(
      response => {
        if(response.status == "success"){
          this._router.navigate(['inicio']);
        }
        else{
          this.status = 'error';
        }

      },
      error => {
        this.status = 'error';

      }
    );
  }

  getNextPost(dir = 'follow'){
    console.log('Dir:' + dir);
    this._postService.getNextPost(this.post.id, dir).subscribe(
      response => {
        console.log(response);
        if(response.status == "success"){
          this.status = 'success';
          this.post = response.post;
  }
        else{
          this._router.navigate(['inicio']);
        }
      },
      error => {
        this.status = "error";
        this._router.navigate(['inicio']);
      }
    );
  }

}
