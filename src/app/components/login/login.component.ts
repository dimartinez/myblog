import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { User } from "../../models/user";
import { UserService } from "../../services/user.service";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {

  public user: User;
  public page_title: string;
  public status: string;
  public errors: string[];
  public token: string;
  public identity: any;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    this.page_title = "Identifícate";
    this.user = new User(1, '', '', 'ROLE_USER', '', '', '', '');

   }

  ngOnInit() {
    //Se ejecuta siempre pero solo cierra sesión cuando la URL dispone del atributo :sure = 1
    this.logout();
  }

  onSubmit(form){
    //Obtener el token de acceso
    this._userService.signup(this.user).subscribe(
      response => {
        if(response.status != 'error'){
          this.status = 'success';
          this.token = response;
          //Ahora obtener la identidad del usuario
          this._userService.signup(this.user, 'false').subscribe(
            response => {
              this.identity = response;
              // Persistir los datos de token e identidad de usuario
              localStorage.setItem('token', this.token);
              localStorage.setItem('identity', JSON.stringify(this.identity));
            },
            error => {
              this.status = 'error';
            }
          );
          form.reset();

          //Redirige al inicio
          this._router.navigate(['inicio']);
        }
        else{
          this.status = 'error';
          this.errors = response.errors;
        }
      },
      error => {
        this.status = 'error';
      }
    );
  }

  logout(){
    this._route.params.subscribe(
      params => {
        let logout = +params['sure'];

        if(logout == 1){
          localStorage.removeItem('identity');
          localStorage.removeItem('token');

          this.identity = null;
          this.token = null;

          this._router.navigate(['inicio']);
        }
      }
    );
  }

}
