import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/user.service";
import { global } from "../../services/global";
import { Post } from '../../models/post';
import { PostService } from "../../services/post.service";

var posts_cache: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [UserService, PostService]
})
export class HomeComponent implements OnInit {

  public posts: Post[];
  public page_title: string;
  public status: string;
  public errors: string[];
  public identity: any;
  public token: string;
  public url: string;

  constructor(
    private _postService: PostService,
    private _userService: UserService

  ) { 
    this.page_title = "Inicio";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = global.url;
  }

  ngOnInit() {
    // Leyendo el caché
    // this.posts = JSON.parse(localStorage.getItem('posts'));
    if(posts_cache) {
      this.posts = JSON.parse(posts_cache);
    }
    // Obtener los datos del servicio y reemplazar los usados en la caché
    this.getPosts();
  }

  getPosts(){
    this._postService.getPosts().subscribe(
      response => {
        if(response.status == "success"){
          this.posts = response.posts;
          // Guardando en la caché
          // localStorage.setItem('posts', JSON.stringify(this.posts));
          posts_cache = JSON.stringify(this.posts);
        }
        else{
          this.status = 'error';
        }
      },
      error => {
        this.status = 'error';
      }
    );
  }

  onDeletePost(id){
    this._postService.delete(this.token, id).subscribe(
      response => {
        if(response.status == "success"){
          this.getPosts();
        }
        else{
          this.status = 'error';
        }

      },
      error => {
        this.status = 'error';

      }
    );
  }

}
