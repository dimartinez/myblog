import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { PostService } from "../../services/post.service";
import { Post } from '../../models/post';
import { UserService } from "../../services/user.service";
import { User } from 'src/app/models/user';
import { global } from "../../services/global";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [PostService, UserService]
})
export class ProfileComponent implements OnInit {
  public status: string;
  public posts: Post[];
  public url: string;
  public identity: any;
  public user: User;


  constructor(
    private _postService: PostService,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router

  ) {
    this.url = global.url;
    this.identity = _userService.getIdentity();
    this.user = null;

   }

  ngOnInit() {
    this.getProfile();
  }

  getProfile(){
    this._route.params.subscribe(
      params => {
        let userId = +params['id'];

        if(userId > 0){
          this._userService.getUserDetail(userId).subscribe(
            responseUser => {
              if(responseUser.status == 'success'){
                this.status = 'success';
                this.user = responseUser.user;
                this.posts = null;
                this._postService.getPostByUser(userId).subscribe(
                  responsePosts => {
                    this.status = 'success';
                    this.posts = responsePosts.posts;
                  },
                  error => {
                    this.status = 'error';

                  }
                );
              }
              else{
                this.status = 'error';
                this._router.navigate(['inicio']);
              }
            },
            error => {
              this.status = 'error';
              this._router.navigate(['inicio']);
            }
          );
      
        }
        else{
          this.status = 'error';
          this._router.navigate(['inicio']);  
        }
      },
      error => {
        this.status = 'error';
        this._router.navigate(['inicio']);
      }
    );
  }

}
