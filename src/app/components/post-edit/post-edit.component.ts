import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/user.service";
import { global } from "../../services/global";
import { Post } from '../../models/post';
import { CategoryService } from '../../services/category.service';
import { PostService } from "../../services/post.service";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Category } from '../../models/category';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css'],
  providers: [UserService, CategoryService, PostService]
})
export class PostEditComponent implements OnInit {

  public post: Post;
  public page_title: string;
  public status: string;
  public errors: string[];
  public identity: any;
  public token: string;
  public url: string;
  public froala_options: Object;
  public afuConfig: Object;
  public categories: Category[];

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _categoryService: CategoryService,
    private _postService: PostService

  ) { 
    this.page_title = "Actualizar entrada";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = global.url;

    this.froala_options = {
      charCounterCount: true,
      toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
      toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
      toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
      toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
    };
    this.afuConfig = {
      multiple: false,
      formatsAllowed: ".jpg,.png,.jpeg",
      maxSize: "20",
      uploadAPI:  {
        url: global.url + 'post/upload',
        headers: {
          "Authorization" : this._userService.getToken()
        }
      },
      theme: "attachPin",
      hideProgressBar: false,
      hideResetBtn: true,
      hideSelectBtn: false,
      replaceTexts: {
        selectFileBtn: 'Seleccionar Imágenes',
        resetBtn: 'Restablecer',
        uploadBtn: 'Cargar',
        dragNDropBox: 'Arrastrar y Soltar',
        attachPinBtn: 'Subir la imagen del post',
        afterUploadMsg_success: 'Imagen cargada exitosamente!',
        afterUploadMsg_error: 'Carga de archivo fallido !'
      }
    };

  }

  ngOnInit() {
    this.getCategories();
    this.post = new Post(1,1,1,'','',null,null);
    this.getPost();
  }

  getPost(){
    this._route.params.subscribe(
      params => {
        let id = +params['id'];

        if(id > 0){
          this._postService.getPost(id).subscribe(
            response => {
              if(response.status == 'success'){
                if(response.post.user_id == this.identity.sub){
                  this.post.id = response.post.id;
                  this.post.title = response.post.title;
                  this.post.content = response.post.content;
                  this.post.image = response.post.image;
                  this.post.category_id = response.post.category_id;
  
                }
                else{
                  this.status = 'error';
                  this._router.navigate(['inicio']);
  
                }
              }
              else{
                this.status = 'error';
                this._router.navigate(['inicio']);
              }
            },
            error => {
              this.status = 'error';
              this._router.navigate(['inicio']);
            }
          );
      
        }
        else{
          this.status = 'error';
          this._router.navigate(['inicio']);  
        }
      },
      error => {
        this.status = 'error';
        this._router.navigate(['inicio']);
      }
    );

  }

  onSubmit(form){
    this._postService.update(this.token, this.post, this.post.id).subscribe(
      response => {
        if(response.status == 'success'){
          this.status = 'success';
          
          // Redirección
          this._router.navigate(['entrada', this.post.id]);
        }
        else{
          this.status = 'success';

        }
      },
      error => {
        this.status = 'error';

      }
    );
  }

  getCategories(){
    this._categoryService.getCategories().subscribe(
      response => {
        if(response.status == "success"){
          this.categories = response.categories;
        }
        else{
          this.status = 'error';
        }
      },
      error => {
        this.status = 'error';
      }
    );
  }

  imageUpload(datos){
    let data = JSON.parse(datos.response);
    this.post.image = data.image;
  }

}
