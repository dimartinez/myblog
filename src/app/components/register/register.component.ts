import { Component, OnInit } from '@angular/core';
import { User } from "../../models/user";
import { UserService } from "../../services/user.service";

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService]
})
export class RegisterComponent implements OnInit {

  public user: User;
  public page_title: string;
  public status: string;
  public errors: string[];

  constructor(
    private _userService: UserService
  ) {
    this.user = new User(1, '', '', 'ROLE_USER', '', '', '', '');
    this.page_title = "Regístrate";
  }

  ngOnInit() {
    // console.log("Cargando componente register");
    // console.log(this._userService.test());
  }
  
  onSubmit(form){
    this._userService.register(this.user).subscribe(
      response => {
        if(response.status == "success"){
          this.status = 'success';

          form.reset();
        }
        else{
          this.status = 'error';
          this.errors = response.errors;
        }
      },
      error => {
        this.status = 'error';
      }
    );
  }
}
