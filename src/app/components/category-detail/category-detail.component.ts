import { Component, OnInit } from '@angular/core';
import { CategoryService } from "../../services/category.service";
import { PostService } from "../../services/post.service";
import { Category } from '../../models/category';
import { Post } from '../../models/post';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { UserService } from "../../services/user.service";
import { global } from "../../services/global";

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css'],
  providers: [CategoryService, PostService, UserService]
})
export class CategoryDetailComponent implements OnInit {
  public status: string;
  public category: Category;
  public posts: Post[];
  public url: string;
  public identity: any;
  public token: string;


  constructor(
    private _categoryService: CategoryService,
    private _postService: PostService,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router

  ) {
    this.url = global.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();

  }

  ngOnInit() {
    this.getCategory();
  }

  getCategory(){
    this._route.params.subscribe(
      params => {
        let id = +params['id'];

        if(id > 0){
          this._categoryService.getCategory(id).subscribe(
            response => {
              if(response.status == 'success'){
                this.status = 'success';
                this.category = response.category;
                this.posts = null;
                this._postService.getPostByCategory(id).subscribe(
                  response => {
                    this.status = 'success';
                    this.posts = response.posts;
                  },
                  error => {
                    this.status = 'error';

                  }
                );
              }
              else{
                this.status = 'error';
                this._router.navigate(['inicio']);
              }
            },
            error => {
              this.status = 'error';
              this._router.navigate(['inicio']);
            }
          );
      
        }
        else{
          this.status = 'error';
          this._router.navigate(['inicio']);  
        }
      },
      error => {
        this.status = 'error';
        this._router.navigate(['inicio']);
      }
    );
  }

  deleteCategory(category_id){
    if(category_id > 0){
      this._categoryService.deleteCategory(this.token, category_id).subscribe(
        response => {
          if(response.status == 'success'){
            this._router.navigate(['inicio']);

          }
          else{
            this._router.navigate(['categoria/', category_id]);
            console.log("Error al intentar borrar la categoría " + category_id + response.message);

          }
        },
        error => {
          console.log("Error al intentar borrar la categoría " + category_id + error.message);
        }
      );

    }

  }
}
