export var global = {
    url: "http://dockerlabs.metadockit.com/api/",
      replaceAnds: function (str: string) {
        return String(str)
                        .replace(/&amp;/g, '(%Y%)')
                        .replace(/&#0*38;/g, '(%Y%)')
                        .replace(/&/g, '(%Y%)');
      }

}